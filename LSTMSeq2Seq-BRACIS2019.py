from hyperopt import fmin, tpe, hp, STATUS_OK, Trials, space_eval
import numpy as np
from pandas import read_csv, to_datetime, DataFrame, concat
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import MinMaxScaler
from keras.models import Model
from keras.layers import Input
from keras.layers import Dense
from keras.layers import RepeatVector
from keras.layers.wrappers import TimeDistributed
from keras.layers import Dropout
from keras.layers.recurrent import LSTM
import time

# difference dataset
def difference(data, order):
	return [data[i] - data[i - order] for i in range(order, len(data))]

# mean absolute error
def measure_mae(actual, predicted):
    return mean_absolute_error(actual,predicted)

# forecast with the best fit model
def model_predict(model, X_test, n_steps_in, n_features, n_diff):

    correction = 0.0
    if n_diff > 0:
        correction = X_test[-n_diff]
        X_test = difference(X_test, n_diff)
    # reshape sample into [samples, timesteps, features]
    # x_input = array(history[-n_input:]).reshape((1, n_input, 1))
    X_test = X_test.reshape((1, n_steps_in, n_features))
    # forecast
    yhat = model.predict(X_test, verbose=0)
    return correction + yhat[0]


def data(): #uf,quarters,scale,n_steps_in,n_steps_out,n_test,n_features
    
    file = None
    
    # load dataset
    if uf=='AL':
        file = 'tsAL.csv'
    elif uf=='AP':
        file = 'tsAP.csv'
    elif uf=='CE':
        file = 'tsCE.csv'
    elif uf=='DF':
        file = 'tsDF.csv'
    elif uf=='PA':
        file = 'tsPA.csv'
    elif uf=='PB':
        file = 'tsPB.csv'
    elif uf=='PE':
        file = 'tsPE.csv'
    elif uf=='RN':
        file = 'tsRN.csv'
    elif uf=='TO':
        file = 'tsTO.csv'
           
        
    ts_df = read_csv(file, sep=';')
    ts_df['Unnamed: 0'] = to_datetime(ts_df['Unnamed: 0'])
    ts_df.index = ts_df['Unnamed: 0']
    del ts_df['Unnamed: 0']
    
    # Retrieve data from all drugs
    if quarter==1:
        tsdata = ts_df['1/1/2013':'3/1/2018'].iloc[:,:].values.astype('float64')
    elif quarter==2:
        tsdata = ts_df['1/1/2013':'6/1/2018'].iloc[:,:].values.astype('float64')
    elif quarter==3:
        tsdata = ts_df['1/1/2013':'9/1/2018'].iloc[:,:].values.astype('float64')
        
    # Number of features (number of multiple inputs or drugs)
    n_features =  tsdata.shape[1] # or 
    
    
    # prepare data
    # rescale values to -1, 1
    if scale:
        scaler = MinMaxScaler(feature_range=(-1, 1))
        scaled_data = scaler.fit_transform(tsdata)
    else:
        scaled_data = tsdata
    
    # split into train and test samples
    X, y = list(), list()
  
    for i in range(len(scaled_data)):
        # find the end of this pattern
        end_ix = i + n_steps_in
        out_end_ix = end_ix + n_steps_out
        # check if we are beyond the sequence
        if out_end_ix > len(scaled_data):
            break
        # gather input and output parts of the pattern
        seq_x, seq_y = scaled_data[i:end_ix], scaled_data[end_ix:out_end_ix]
        X.append(seq_x)
        y.append(seq_y)
    
    X = np.array(X)
    y = np.array(y)

    # train and test samples
    X_train, y_train = X[0:-n_test,:,:], y[0:-n_test,:,:]
    X_test, y_test = X[-n_test:,:,:], y[-n_test:,:,:]   
    
    return X_train, X_test, y_train, y_test, scaler, n_features, ts_df.columns #CATMAT


def objective(args):
 
    n_units_1 = args['n_units_1']
    n_units_2 = args['n_units_2']
    epochs = args['epochs']
    drop_rate = args['dropout']
    
   
    #define model
    inputs = Input(shape=(n_steps_in, n_features))
    drop_1 = Dropout(drop_rate)(inputs, training=True)
    lstm_1 = LSTM(n_units_1, activation= 'relu')(drop_1)
    drop_2 = Dropout(drop_rate)(lstm_1, training=True)
    repeat_vector = RepeatVector(n_steps_out)(drop_2)
    lstm_2 = LSTM(n_units_2, activation= 'relu', return_sequences=True)(repeat_vector)
    drop_3 = Dropout(drop_rate)(lstm_2, training=True)
    outputs = TimeDistributed(Dense(n_features))(drop_3)
    model = Model(inputs=inputs, outputs=outputs)
    
    # compile model
    model.compile(optimizer='adam', loss='mae', metrics=['mae'])
    
    # fit model
    model.fit(X_train, 
              y_train, 
              #epochs={{choice([50, 100, 200, 500, 1000])}}, 
              epochs=epochs, 
              #callbacks=callbacks, # Early stopping
              verbose=0)
    
    score, acc = model.evaluate(X_test, y_test, verbose=0)
    
    return {'loss': acc, 'status': STATUS_OK, 'model': model}


def unpack_hyperopt_vals(vals):
    """
    Unpack values from a hyperopt return dictionary where values are wrapped in a list.
    :param vals: dict
    :return: dict
        copy of the dictionary with unpacked values
    """
    assert isinstance(vals, dict), "Parameter must be given as dict."
    ret = {}
    for k, v in list(vals.items()):
        try:
            ret[k] = v[0]
        except (TypeError, IndexError):
            ret[k] = v
    return ret


def eval_hyperopt_space(space, vals):
    """
    Evaluate a set of parameter values within the hyperopt space.
    Optionally unpacks the values, if they are wrapped in lists.
    :param space: dict
        the hyperopt space dictionary
    :param vals: dict
        the values from a hyperopt trial
    :return: evaluated space
    """
    unpacked_vals = unpack_hyperopt_vals(vals)
    return space_eval(space, unpacked_vals)

####################################################################################################
# MAIN LOOP
# Change ufs tuple accordingly before running
# Possible values: "DF", "CE", "PE", "AL", "AP", "PA", "TO", "RN" or "PB" 
#    
####################################################################################################
if __name__ == "__main__":
    
    np.random.seed(28042019)  # for reproducibility
    
    list_tsteps = list()
    
    df_MAE, df_totalMAE =  DataFrame(), DataFrame()
    
    df_FCAST, df_FCAST_IC_60_INF, df_FCAST_IC_60_SUP = DataFrame(), DataFrame(), DataFrame()
    
    quarters = tuple([1,2,3])
    ufs = tuple(['DF'])
    tsteps_in = tuple([1,3,6,12])
    
    n_steps_out = 4
    n_test = 1
    n_evals = 10
    scale = True
    
    # Parameter Space
    space_params = {'n_units_1': hp.choice('n_units_1', [12,32,64,128]),
                    'n_units_2': hp.choice('n_units_2', [12,32,64,128]),
                    'epochs'   : hp.choice('epochs', [100,200,500,1000,1500]),
                    'dropout'  : hp.choice('dropout', [0.005,0.01,0.05,0.1])}
   
    
          
    # main loop
    start = time.time()
    for quarter in quarters:
        for uf in ufs:
            for n_steps_in in tsteps_in:
                
                                
                X_train, X_test, y_train, y_test, scaler, n_features, catmat = data()
                
                    
   
                trials=Trials()
                best_run = fmin(objective, 
                                space=space_params, 
                                algo=tpe.suggest, 
                                max_evals=n_evals, 
                                trials=trials)
                
                print('Optimizing ....')

                # get the best model
                # copied from https://github.com/maxpumperla/hyperas/blob/master/hyperas/optim.py
                best_model = None
                for trial in trials:
                    vals = trial.get('misc').get('vals')
                    # unpack the values from lists without overwriting the mutable dict within 'trial'
                    unpacked_vals = unpack_hyperopt_vals(vals)
                    # identify the best_run (comes with unpacked values from the hyperopt function `base.Trials.argmin`)
                    if unpacked_vals == best_run and 'model' in trial.get('result').keys():
                        best_model = trial.get('result').get('model')
        
                print('UF: ' + ufs[0] + ', ' + 'Quarter: ' + str(quarter) +  ', ' + 'Time Steps: ' + str(n_steps_in))
                best_params = space_eval(space_params,trials.argmin)
                print(best_params)
                             
              
                
                # Monte Carlo Dropout for measuring uncertainty (see Yarin Gal 2016 thesis)
                T = 10000
                y_hat = np.array([best_model.predict(X_test, verbose=0) for _ in range(T)])
                
                #reshape y_hat from 4D to 3D  (10000 x 4 x med)
                y_hat = y_hat.reshape((y_hat.shape[0], y_hat.shape[2], y_hat.shape[3]))
                
                #reshape y_test from 3D to 2D 
                y_test = y_test.reshape((y_test.shape[1], y_test.shape[2]))                
                
                
                # invert scale
                if scale:
                    y_hat_temp = np.array([scaler.inverse_transform(y_hat[i,:,:]) for i in range(T)])
                    y_hat = y_hat_temp
                    y_test = scaler.inverse_transform(y_test)
            
                         
                # y_hat MEAN (4 x med)
                y_hat_mean = np.mean(y_hat, axis=0)
                
                # y_hat AGGREGATE (10000 x med)
                y_hat_agg = y_hat.sum(axis=1)
                
                # y_hat AGGREGATE mean (1 x med)
                y_hat_agg_mean = np.mean(y_hat_agg, axis=0)
                
                # y_hat AGGREGATE IC 60% (2 x med) 
                y_hat_ic_60_agg = np.quantile(y_hat_agg, q=[0.2,0.8], axis=0)
                
                
                
                # Mean Absolute Error
                error_avg = mean_absolute_error(y_hat_mean, y_test, multioutput = 'raw_values')
                error_avg = error_avg.round(decimals=2)
                            
                error_per_fcast = abs(y_hat_mean-y_test)
                error_per_fcast = error_per_fcast.round(decimals=2)
                
                # concatenate forecast errors for each month in horizon and average error
                error_total = np.concatenate([error_per_fcast, error_avg.reshape(1,n_features)], axis=0)
                            
                                
                # save best hyperparameters
                list_tsteps.append({'QUARTER':quarter, 
                                     'UF':uf, 
                                     'STEPS_IN':n_steps_in,
                                     'EPOCHS':best_params['epochs'],
                                     'N_UNITS_1':best_params['n_units_1'],
                                     'N_UNITS_2':best_params['n_units_2'],
                                     'DROPOUT':best_params['dropout']})
                
                # update MAE and totalMAE      
                df_MAE = concat([df_MAE,DataFrame(error_avg)], axis=1)
                df_totalMAE = concat([df_totalMAE,DataFrame(error_total)], axis=0)
                
                # update FCAST_MEAN
                df_FCAST = concat([df_FCAST,DataFrame(y_hat_mean)], axis=1)
                
                # update FCAST_IC_60_SUP
                df_FCAST_IC_60_SUP = concat([df_FCAST_IC_60_SUP, DataFrame(y_hat_ic_60_agg[1,:])], axis=1)
                
                # update FCAST_IC_60_INF
                df_FCAST_IC_60_INF = concat([df_FCAST_IC_60_INF, DataFrame(y_hat_ic_60_agg[0,:])], axis=1)
    
    print('Total optimization time: %.2f seconds' %(time.time()-start))
    print('End Main Loop')
    
    
    
###################################################################################################
# SAVE DATA 
# CSV files with MAE and IC 60 predictios will be loaded by R script
###################################################################################################
     
    #save list of hyperparameters to a Data Frame and *.csv file
    df_results = DataFrame.from_records(list_tsteps)
    df_results = df_results[['QUARTER', 'UF', 'STEPS_IN',
                             'EPOCHS','N_UNITS_1','N_UNITS_2','DROPOUT']]
 
    df_results.to_csv('LSTM_BOpt_' + uf + '_CONFIGS.csv', header=True) 

    
    #save MAE DataFrame
    df_MAE = df_MAE.T
    df_MAE.columns = catmat.values
    df_MAE.reset_index(drop=True, inplace=True)
    df_MAE.to_csv('LSTM_BOpt_' + uf + '_MAE' + '.csv', header=True)

    
    #save FCAST DataFrame
    nmed = n_features
   
    
    df_temp = concat([df_FCAST.iloc[:,0:nmed],df_FCAST.iloc[:,nmed:2*nmed],
                      df_FCAST.iloc[:,2*nmed:3*nmed],df_FCAST.iloc[:,3*nmed:4*nmed],
                      df_FCAST.iloc[:,4*nmed:5*nmed],df_FCAST.iloc[:,5*nmed:6*nmed],
                      df_FCAST.iloc[:,6*nmed:7*nmed],df_FCAST.iloc[:,7*nmed:8*nmed],
                      df_FCAST.iloc[:,8*nmed:9*nmed],df_FCAST.iloc[:,9*nmed:10*nmed],
                      df_FCAST.iloc[:,10*nmed:11*nmed],df_FCAST.iloc[:,11*nmed:12*nmed]])
    
    df_temp.columns = catmat.values
    df_FCAST = df_temp
    df_FCAST.reset_index(drop=True, inplace=True)
    df_FCAST.to_csv('LSTM_BOpt_' + uf + '_FCAST' + '.csv', header=True)
   
   
    df_temp = concat([df_FCAST_IC_60_INF.iloc[:,0],df_FCAST_IC_60_SUP.iloc[:,0],
                      df_FCAST_IC_60_INF.iloc[:,1],df_FCAST_IC_60_SUP.iloc[:,1],
                      df_FCAST_IC_60_INF.iloc[:,2],df_FCAST_IC_60_SUP.iloc[:,2],
                      df_FCAST_IC_60_INF.iloc[:,3],df_FCAST_IC_60_SUP.iloc[:,3],
                      df_FCAST_IC_60_INF.iloc[:,4],df_FCAST_IC_60_SUP.iloc[:,4],
                      df_FCAST_IC_60_INF.iloc[:,5],df_FCAST_IC_60_SUP.iloc[:,5],
                      df_FCAST_IC_60_INF.iloc[:,6],df_FCAST_IC_60_SUP.iloc[:,6],
                      df_FCAST_IC_60_INF.iloc[:,7],df_FCAST_IC_60_SUP.iloc[:,7],
                      df_FCAST_IC_60_INF.iloc[:,8],df_FCAST_IC_60_SUP.iloc[:,8],
                      df_FCAST_IC_60_INF.iloc[:,9],df_FCAST_IC_60_SUP.iloc[:,9],
                      df_FCAST_IC_60_INF.iloc[:,10],df_FCAST_IC_60_SUP.iloc[:,10],
                      df_FCAST_IC_60_INF.iloc[:,11],df_FCAST_IC_60_SUP.iloc[:,11]],axis=1)
    

    
    df_temp = df_temp.T
    
    df_temp.columns = catmat.values
    df_FCAST_IC_60_INF_SUP = df_temp
    df_FCAST_IC_60_INF_SUP.reset_index(drop=True, inplace=True)
    df_FCAST_IC_60_INF_SUP.to_csv('LSTM_BOpt_' + uf + '_FCAST_IC60' + '.csv', header=True)
    del df_temp
    


  
    # SAVE BEST FCAST IC 60 PER QUARTER
    df_totalMAE.reset_index(drop=True, inplace=True)
    df_MAE.reset_index(drop=True, inplace=True)
    
    # FIRST QUARTER
    idx_q = df_MAE.iloc[0:4,:].idxmin(axis=0).values * 2 # inf and sup
    idx_q_mae = (df_totalMAE.iloc[[4,9,14,19],:].idxmin(axis=0).values // 5) * 5
    
    df_inf_sup_q1 = DataFrame()
    df_mae_q1 = DataFrame()
    
    for med in range(nmed):
        # IC 60 INF + SUP
        df_temp = df_FCAST_IC_60_INF_SUP.iloc[idx_q[med]:idx_q[med]+2,med]
        df_temp.reset_index(drop=True,inplace=True)
        df_inf_sup_q1 = concat([df_inf_sup_q1,df_temp], axis=1)
       
        # POINT AND AVERAGE MAE
        df_temp = df_totalMAE.iloc[idx_q_mae[med]:idx_q_mae[med]+5,med]
        df_temp.reset_index(drop=True,inplace=True)
        df_mae_q1 = concat([df_mae_q1,df_temp], axis=1)
    #end for
    df_inf_sup_q1.reset_index(drop=True, inplace=True)

    
    # SECOND QUARTER
    idx_q = df_MAE.iloc[4:8,:].idxmin(axis=0).values*2
    idx_q_mae = df_totalMAE.iloc[[24,29,34,39],:].idxmin(axis=0).values//5*5
    
    df_inf_sup_q2 = DataFrame()
    df_mae_q2 = DataFrame()
   
    for med in range(nmed):
        # IC 60 INF + SUP
        df_temp = df_FCAST_IC_60_INF_SUP.iloc[idx_q[med]:idx_q[med]+2,med]
        df_temp.reset_index(drop=True,inplace=True)
        df_inf_sup_q2 = concat([df_inf_sup_q2,df_temp], axis=1)
                
        # POINT AND AVERAGE MAE
        df_temp = df_totalMAE.iloc[idx_q_mae[med]:idx_q_mae[med]+5,med]
        df_temp.reset_index(drop=True,inplace=True)
        df_mae_q2 = concat([df_mae_q2,df_temp], axis=1)
    #end for
    df_inf_sup_q2.reset_index(drop=True, inplace=True)


    # THIRD QUARTER
    idx_q = df_MAE.iloc[8:12,:].idxmin(axis=0).values*2
    idx_q_mae = df_totalMAE.iloc[[44,49,54,59],:].idxmin(axis=0).values//5*5
    
    df_inf_sup_q3 = DataFrame()
    df_mae_q3 = DataFrame()
   
    for med in range(nmed):
        # IC 60 INF + SUP
        df_temp = df_FCAST_IC_60_INF_SUP.iloc[idx_q[med]:idx_q[med]+2,med]
        df_temp.reset_index(drop=True,inplace=True)
        df_inf_sup_q3 = concat([df_inf_sup_q3,df_temp], axis=1)
        
        # POINT (fh=4) AND AVERAGE MAE
        df_temp = df_totalMAE.iloc[idx_q_mae[med]:idx_q_mae[med]+5,med]
        df_temp.reset_index(drop=True,inplace=True)
        df_mae_q3 = concat([df_mae_q3,df_temp], axis=1)
    #end for        
        
    df_inf_sup_q3.reset_index(drop=True, inplace=True)
    
         
    #  Save IC 60 forecasts [2(inf,sup)*nquarters x nmed] 
    df_save_ic = concat([df_inf_sup_q1, df_inf_sup_q2, df_inf_sup_q3], axis=0)
    df_save_ic.to_csv('LSTM_BOpt_' + uf + '_FCAST_IC60_INF_SUP.csv', header=True)

    #  Save point and average MAE [5 (fh + avg) x nmed*nquarters]
    df_save_mae = concat([df_mae_q1, df_mae_q2, df_mae_q3], axis=1)
    df_save_mae.to_csv('LSTM_BOpt_' + uf + '_TOTAL_MAE.csv', header=True)
