library(forecast)
library(tseries)
library(urca)
library(pracma)
library(xts)
library(dplyr)
library(tidyr)


# Cox-Stuart Test
o.Cox.Stuart.test <- function(x, alternative=c("two.sided" ,"left.sided", "right.sided")){
  
  dname <- deparse(substitute(x))
  
  alternative <- match.arg(alternative)
  
  stopifnot(is.numeric(x))
  
  n0 <- length(x)
  
  if (n0 < 2){
    stop("sample size must be greater than 1")
  }
  
  n0 <- round(length(x)) %% 2
  
  if (n0 == 1) {
    remove <- (length(x)+1)/2
    x <- x[ -remove ] 
  }
  
  half <- length(x)/2
  x1 <- x[1:half]
  x2 <- x[(half+1):(length(x))]
  n <- sum((x2-x1)!=0)
  t <- sum(x1<x2)
  
  if (alternative == "left.sided") {
    p.value <- pbinom(t, n, 0.5)
    alternative <- "decreasing trend"
  }
  
  if (alternative == "right.sided"){
    p.value <- 1-pbinom(t-1, n, 0.5)
    alternative <- "increasing trend"
  }
  
  if (alternative == "two.sided"){
    alternative <- "any type of trend, either decreasing or increasing" 
    if (1-pbinom(t-1, n, 0.5) == pbinom(t, n, 0.5)) {
      pdist <- dbinom(0:n, n, 0.5)
      p.value <- sum(pdist[pdist <= t+1])
    }
    else {
      p.value <- 2*min( 1-pbinom(t-1, n, 0.5), pbinom(t, n, 0.5))
    } 
  }
  
  rval <- list(statistic=c("Test Statistic"=t), 
               alternative=alternative, 
               p.value=p.value, 
               method="Cox & Stuart Test for Trend Analysis", 
               parameter=c("Number of Untied Pairs"=n),
               data.name=dname)
  
  class(rval) <- "htest"
  return(rval)
}

# Mean Absolute Error 
mae_cal <- function(outsample, forecasts){
  #Used to estimate MAE
  outsample <- as.numeric(outsample) ; forecasts<-as.numeric(forecasts)
  mae <- as.matrix(abs(outsample-forecasts))
  #add a column average MAE
  mae <- as.numeric(round(rbind(mae,mean(mae)),3))
  return(mae)
  #return(abs(outsample-forecasts))
}
# end of mae_cal() function

# naive_seasonal <- function(input, fh){
#   #Used to estimate Seasonal Naive
#   frcy <- frequency(input$x)
#   frcst <- input$mean 
#   if (frcy>1){ 
#     # last fh values of the last year
#     input$mean <- head(rep(as.numeric(tail(input$x,frcy)), fh), fh) + frcst - frcst
#   }
#   return(input)  
# }

# Test series seasonality
seasonalityTest <- function(input, ppy){
  #Used to determine whether a time series is seasonal
  tcrit <- 1.645
  if (length(input)<3*ppy){
    test_seasonal <- FALSE
  }else{
    xacf <- acf(input, plot = FALSE)$acf[-1, 1, 1]
    clim <- tcrit/sqrt(length(input)) * sqrt(cumsum(c(1, 2 * xacf^2)))
    test_seasonal <- ( abs(xacf[ppy]) > clim[ppy] )
    #print("acf...")
    if (is.na(test_seasonal)==TRUE){ test_seasonal <- FALSE }
  }
  
  return(test_seasonal)
}
# end of seasonalityTest() function

# Function to train models. Return models forecast object and additional information
statModels <- function(input, des_input, des_det_input, fh) {
  # MODELS TRANING
  #fcast.naive <- naive(input, lambda="auto", biasadj = TRUE, h=fh, level=c(60,80,95))
  #fcast.snaive <- snaive(input, lambda="auto", biasadj = TRUE, h=fh, level=c(60,80,95))
  fcast.naive <- naive(input, h=fh, level=c(50,60,70,80,95))
  fcast.snaive <- snaive(input, h=fh, level=c(50,60,70,80,95))
  #fcast.thetaf <- stlf(input, forecastfunction=thetaf, h=fh, level=c(60,80))
  fcast.thetaf <- thetaf(des_input, h=fh, level=c(50,60,70,80,95))
  
  # Methods which requires a seasonally adjusted component
  #fcast.ses <- stlf(input, forecastfunction=ses, h=fh, level=c(60,80))
  #fcast.ses <- ses(des_input, lambda="auto", biasadj = TRUE, h=fh,level=c(60,80,95))
  fcast.ses <- ses(des_input, h=fh,level=c(50,60,70,80,95))
  #fcast.holt.linear <- stlf(input, forecastfunction=holt, h=fh, level=c(60,80))
  #fcast.holt.linear <- holt(des_input, lambda="auto", biasadj = TRUE, h=fh, damped=FALSE,level=c(60,80,95))
  fcast.holt.linear <- holt(des_input, h=fh, damped=FALSE,level=c(50,60,70,80,95))
  #fcast.holt.damped <- stlf(input, forecastfunction=holt, damped=TRUE, h=fh, level=c(60,80))
  #fcast.holt.damped <-holt(des_input, lambda="auto", biasadj = TRUE, h=fh, damped=TRUE,level=c(60,80,95))
  fcast.holt.damped <-holt(des_input, h=fh, damped=TRUE,level=c(50,60,70,80,95))
  
  #fcast.ets.deseasonality <- stlf(ts_MED_train, s.window=3, method='ets', h=fh, level=c(60,80))
  
  # Methods which deals with trended and/or seasonal time series
  #fcast.holt.winters <- hw(input, lambda="auto", biasadj = TRUE, h=fh, level=c(60,80,95))
  fcast.holt.winters <- hw(input, h=fh, level=c(50,60,70,80,95))
  #fcast.arima <- forecast(auto.arima(input, lambda="auto", biasadj = TRUE, ic='aicc', stepwise=FALSE), h=fh, level=c(60,80,95))
  fcast.arima <- forecast(auto.arima(input, ic='aicc', stepwise=FALSE), h=fh, level=c(50,60,70,80,95))
  #fcast.ets <- forecast(ets(input, lambda="auto", biasadj = TRUE, ic='aicc'), h=fh, level=c(60,80,95))
  fcast.ets <- forecast(ets(input, ic='aicc'), h=fh, level=c(50,60,70,80,95))
  
  
  # #Combined (SES + HOLT LINEAR + HOLT DAMPED)
  # comb.model <- list(mean=(fcast.ses$mean+fcast.holt.linear$mean+fcast.holt.damped$mean)/3,
  #                    lower=(fcast.ses$lower+fcast.holt.linear$lower+fcast.holt.damped$lower)/3,
  #                    upper=(fcast.ses$upper+fcast.holt.linear$upper+fcast.holt.damped$upper)/3)
  # 
  # attributes(comb.model$lower)[[4]][[2]][1] <- "Lo 60"
  # attributes(comb.model$lower)[[4]][[2]][2] <- "Lo 80"
  # attributes(comb.model$upper)[[4]][[2]][1] <- "Hi 60"
  # attributes(comb.model$upper)[[4]][[2]][2] <- "Hi 80"
  
  # List containing all models forecast objects and aditional information (transformation, seasonality and trend)
  return(list(NAIVE=fcast.naive, SNAIVE=fcast.snaive, SES=fcast.ses, 
              HOLT_LINEAR=fcast.holt.linear, HOLT_DAMPED=fcast.holt.damped, 
              THETA=fcast.thetaf, HOLT_WINTERS = fcast.holt.winters,
              ARIMA=fcast.arima, ETS=fcast.ets))
}  
# end of statModels() function


# Function to load LSTM results from CSV files
loadLSTMResults <- function(UF) {
  
  switch(UF,
         DF = {ic_60 <- read.csv('LSTM_BOpt_DF_FCAST_IC60_INF_SUP.csv', stringsAsFactors = FALSE)
              mae <- read.csv('LSTM_BOpt_DF_TOTAL_MAE.csv', stringsAsFactors = FALSE)
         },
         
         # CE = {ic_60 <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_CE_FCAST_IC60_INF_SUP.csv', stringsAsFactors = FALSE)
         #      mae <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_CE_TOTAL_MAE.csv', stringsAsFactors = FALSE)
         # },
         # 
         # PE = {ic_60 <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_PE_FCAST_IC60_INF_SUP.csv', stringsAsFactors = FALSE)
         #      mae <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_PE_TOTAL_MAE.csv', stringsAsFactors = FALSE)
         # },
         # 
         # AL = {ic_60 <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_AL_FCAST_IC60_INF_SUP.csv', stringsAsFactors = FALSE)
         #      mae <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_AL_TOTAL_MAE.csv', stringsAsFactors = FALSE)
         # },
         # 
         # AP = {ic_60 <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_AP_FCAST_IC60_INF_SUP.csv', stringsAsFactors = FALSE)
         #      mae <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_AP_TOTAL_MAE.csv', stringsAsFactors = FALSE)
         # },
         # 
         # TO = {ic_60 <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_TO_FCAST_IC60_INF_SUP.csv', stringsAsFactors = FALSE)
         #      mae <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_TO_TOTAL_MAE.csv', stringsAsFactors = FALSE)
         # },
         # 
         # PA = {ic_60 <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_PA_FCAST_IC60_INF_SUP.csv', stringsAsFactors = FALSE)
         #      mae <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_PA_TOTAL_MAE.csv', stringsAsFactors = FALSE)
         # },
         # 
         # RN = {ic_60 <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_RN_FCAST_IC60_INF_SUP.csv', stringsAsFactors = FALSE)
         #      mae <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_RN_TOTAL_MAE.csv', stringsAsFactors = FALSE)
         # },
         # 
         # PB = {ic_60 <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_PB_FCAST_IC60_INF_SUP.csv', stringsAsFactors = FALSE)
         #      mae <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_PB_TOTAL_MAE.csv', stringsAsFactors = FALSE)
         # },
         stop("Enter something that switches me!")
  )
  
  ic_60 <- ic_60 %>% select(-c(X)) %>% t()
  mae <- mae %>% select(-c(X)) %>% t()
  
  return(list("ic60" = ic_60, "mae" = mae))

  
  # if (UFs_vec[uf] == 'AP') {
  #   # AP
  #   lstm_fcast_ic_60 <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_AP_FCAST_IC60_INF_SUP.csv', 
  #                                stringsAsFactors = FALSE)
  #   lstm_fcast_ic_60 <- lstm_fcast_ic_60 %>% select(-c(X)) %>% t()
  #   
  #   lstm_mae <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_AP_TOTAL_MAE.csv', 
  #                        stringsAsFactors = FALSE)
  #   lstm_mae <- lstm_mae %>% select(-c(X)) %>% t()
  # } 
  # else if (UFs_vec[uf] == 'DF') {
  #   lstm_fcast_ic_60 <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_DF_FCAST_IC60_INF_SUP.csv', 
  #                                stringsAsFactors = FALSE)
  #   lstm_fcast_ic_60 <- lstm_fcast_ic_60 %>% select(-c(X)) %>% t()
  #   
  #   lstm_mae <- read.csv('/home/renanms/Documents/Mestrado/Dissertacao/Codigo/LSTM-Py/LSTM_BOpt_DF_TOTAL_MAE.csv', 
  #                        stringsAsFactors = FALSE)
  #   lstm_mae <- lstm_mae %>% select(-c(X)) %>% t()
  # }
}
# end of loadLSTMResults() function


############################################################################################
# LOAD DATA AND VARIABLES DEFINITION
############################################################################################

# read medicines Time Series from disk 
xts_list_med <- readRDS("listMedicinesXTS.rds")

# read vector of index of medicines with best information quality (no missing values)
idxListMedQual <- readRDS("indexOfQualityMedicines.rds")

# UFs vector (Brazilian States)
UFs_vec <- c("DF")


# forecast horizon
fh <- 4

# frequency of the series
frq <- 12

# models' names
namesModels <- c("NAIVE", "SNAIVE", "SES", "HOLT_LINEAR", "HOLT_DAMPED",
                 "THETA", "HOLT_WINTERS", "ARIMA", "ETS", "LSTM")

# list to store 2018 Quarter Results 
quarterResults <- list()

# list to store UF results (MAE and forecasts)
ufResults <- list()

# array to store MAE for each model
totalMAE <- array(NA, dim = c(length(namesModels), fh+1)) # fh + 1 (4 points + mean)

# array to store Forecasts for each model
# 11 [= 5 IC (50,60,70,80,95) * 2 (High,Low) + 1 (point forecast)]
totalFCAST <- array(NA, dim = c(fh, 11, length(namesModels)))

# 5 aggregate forecasts(50,60,70,80,95) * 2(high,low)
aggFCAST <- array(NA, dim = c(2, 5, length(namesModels)))

# lists to store forecasts (point + IC 60%,80%) for each drug
#fCasts <- list()

# list to store time series characsteristics and results
tsMedResults <- list()

# number of states you want to evaluate
#n_UFs = length(UFs_vec)
n_UFs = 1

# decomposition method to be used (STL or CLASSICAL)
decompMethod = "STL"

# Save plots (TRUE) or not (FALSE)
savePlots = FALSE

# First Year
firstYear = 2013
firstMonth = 1

# Last Year
lastYear = 2018
lastMonth = 10

# number of quarters of 2018 to evaluate (1,2 or 3)
#n_Qs = lastMonth%/%3
n_Qs = 3


#############################################################################################
# MAIN LOOP
# System.time() used to estimate computational cost in terms of time
#############################################################################################
#numCores <- 3
#registerDoParallel(numCores)  # use multicore, set to the number of our cores

system.time (
  # Loop to evaluate forecast error of "n_Qs" quarters of 2018 (Jan-Mar + Apr-Jun + Jul-Sep)

  for (quarter in 1:n_Qs) {
  #foreach (quarter = 1:n_Qs) %dopar% {
    
    print(paste0("Q",quarter))
    
    # Loop to run over all UFs
    for (uf in 1:n_UFs) { # ("DF", "CE", "PE", "AL", "AP", "TO", "PA", "RN", "PB")
      
      # print UF
      print(paste0("UF: ",UFs_vec[uf]))
      
      # load Python LSTM results from .csv files
      lstm_results <- loadLSTMResults(UFs_vec[uf])
      lstm_fcast_ic_60 <- lstm_results$ic60
      lstm_mae <- lstm_results$mae
      
      
      # number of drugs (time-series) of each UF 
      nmed <- length(idxListMedQual[[uf]]$IDX_MED_QUAL)

      # ARIMA_MS aggregate IC 60 predictions for all drugs
      switch(quarter,
             arima_MS_pred <- xts_list_med[[uf]]$ARIMA_MS_PREV[idxListMedQual[[uf]]$IDX_MED_QUAL,"Q1"],
             arima_MS_pred <- xts_list_med[[uf]]$ARIMA_MS_PREV[idxListMedQual[[uf]]$IDX_MED_QUAL,"Q2"],
             arima_MS_pred <- xts_list_med[[uf]]$ARIMA_MS_PREV[idxListMedQual[[uf]]$IDX_MED_QUAL,"Q3"],
             arima_MS_pred <- xts_list_med[[uf]]$ARIMA_MS_PREV[idxListMedQual[[uf]]$IDX_MED_QUAL,"Q4"])
      
      
      # Stock information of drugs
      stockMed <- xts_list_med[[uf]]$STOCK[quarter,idxListMedQual[[uf]]$IDX_MED_QUAL]
      
      
      # Loop to run over all medicines
      for (med in 1:nmed) {
        
        # print Medicine
        print(paste0("Drug: ",toString(med)))
        
        # index of the medicine
        idx_MED <- idxListMedQual[[uf]]$IDX_MED_QUAL[med] 
        
        # drug time series as a ts object
        ts_MED <- ts(coredata(xts_list_med[[uf]]$XTS_MED[,idx_MED])[,1], start=firstYear, end=c(lastYear,lastMonth), frequency=frq)
        
        
        #Train/Test Split
        ts_MED_train <- ts_MED %>% head(n=(lastYear-firstYear)*12-1+(quarter-1)*3)
        ts_MED_test <- ts_MED %>% head(n=(lastYear-firstYear)*12-1+(quarter-1)*3+fh) %>% tail(fh)
        
        if (quarter == 1) #Q1
        {
          # Forecast Jan-Mar
          str = c( paste0("dec/",toString(lastYear-1)),
                   paste0("jan/",toString(lastYear)),
                   paste0("fev/",toString(lastYear)),
                   paste0("mar/",toString(lastYear)),
                   "MAE-avg"
          )
        } else if (quarter == 2) { #Q2
          # Forecast Apr-Jun
          str = c( paste0("mar/",toString(lastYear)),
                   paste0("apr/",toString(lastYear)),
                   paste0("may/",toString(lastYear)),
                   paste0("jun/",toString(lastYear)),
                   "MAE-avg")
        } else { #Q3
          # Forecast Jul-Sep
          str = c( paste0("jun/",toString(lastYear)),
                   paste0("jul/",toString(lastYear)),
                   paste0("aug/",toString(lastYear)),
                   paste0("sep/",toString(lastYear)),
                   "MAE-avg")
        } 
        
        # Save time series characteristics (Decomposition + ACF/PACF + Trend and seasonality strength)
        # Explore time series characteristics (Stationarity, Trend, Seasonality)
        if (decompMethod == "CLASSICAL") {
          ts_MED_train %>% decompose() -> ts_MED_decomp
        }  
        else { # STL decomposition
          ts_MED_train %>% stl(s.window=5, robust=TRUE) -> ts_MED_decomp
        }
        
        #Save decomposition plot to file
        if (savePlots) {
          ggsave(
            plot = autoplot(ts_MED_decomp) +
              ggtitle(paste0(xts_list_med[[uf]]$UF,"-",xts_list_med[[uf]]$COD_MED[idx_MED,1])),
            
            filename = paste0(xts_list_med[[uf]]$UF,"-",xts_list_med[[uf]]$COD_MED[idx_MED,1],"-DECOMPOSE.png")
          )
        }
        
        # Unit Root Test (Stationarity)
        # A number of unit root tests are available, which are based on different assumptions and may 
        # lead to conflicting answers. In our analysis, we use the 
        # Kwiatkowski-Phillips-Schmidt-Shin (KPSS) test (Kwiatkowski, Phillips, Schmidt, & Shin, 1992). 
        # In this test, the null hypothesis is that the data are stationary, and we look for evidence 
        # that the null hypothesis is false. Consequently, small p-values (e.g., less than 0.05) 
        # suggest that differencing is required.
        
        # HO = data are stationary
        # HA = data are not stationary
        # test statistic > 0.05 critical value => rejects H0
        
        tsIsStationary <- TRUE
        tsNdiffs <- 0
        unitRootTest <- ur.kpss(ts_MED_train)
        if ( unitRootTest@teststat > unitRootTest@cval[2]) { # not stationary
          tsNdiffs = ndiffs(ts_MED_train)
          print(paste0("no stationary -> ndiffs =  ",toString(tsNdiffs)))
          tsIsStationary <- FALSE
          ts_MED_train %>% diff(differences=tsNdiffs) -> ts_MED_train_diff
        }
        else {
          ts_MED_train_diff <- ts_MED_train
        }
        
        # Save ACF/PACF
        if (savePlots) {
          #ACF
          ggsave(
            plot = ggAcf(ts_MED, lag=36) +
              ggtitle(paste0(xts_list_med[[uf]]$UF,"-",xts_list_med[[uf]]$COD_MED[idx_MED,1])),
            
            filename = paste0(xts_list_med[[uf]]$UF,"-",xts_list_med[[uf]]$COD_MED[idx_MED,1],"-ACF.png")
          )
          
          #PACF
          ggsave(
            plot = ggPacf(ts_MED, lag=36) +
              ggtitle(paste0(xts_list_med[[uf]]$UF,"-",xts_list_med[[uf]]$COD_MED[idx_MED,1])),
            
            filename = paste0(xts_list_med[[uf]]$UF,"-",xts_list_med[[uf]]$COD_MED[idx_MED,1],"-PACF.png")
          )
        }
        
        
        # Seasonality checking using ACF
        # Steps in case seasonality is present
        # 1. Series decomposition
        # 2. Seasonaly adjust series
        # 3. Calculates seasonal index
        # 4. Fit models and make forecasts using the adjusted series
        # 6. Adjusts predictions using the seasonal index
        tsHasSeasonality <- FALSE
        tsFseasonality <- NULL
        tsHasSeasonality <- seasonalityTest(ts_MED_train,frq)
        if (tsHasSeasonality) {
          print("seasonality detected ...")
          # Seasonality strength (see Hyndman`s chapter 5)
          tsFseasonality = max(0,1-var(remainder(ts_MED_decomp))/var(seasonal(ts_MED_decomp)+remainder(ts_MED_decomp)))
          tsNSdiffs = nsdiffs(ts_MED_train)
          ts_MED_train_deseason <- seasadj(ts_MED_decomp)
          SIout <- sindexf(ts_MED_decomp,fh)
        }
        else {
          #print("no seasonal...")
          ts_MED_train_deseason <- ts_MED_train
          tsNSdiffs <- 0
          SIout <- rep(1, fh)
        }
        
        
        # Trend 
        # Cox-Stuart statistical test to check trend
        # HO = no trend
        # HA = trend
        # p-value < 0.05 rejects HO
        tsHasTrend <- FALSE
        tsFtrend <- NULL
        if (o.Cox.Stuart.test(ts_MED_train_deseason, alternative = "two.sided")$p.value < 0.05) {
          print("trend detected ...")
          tsHasTrend <- TRUE
          # Trend strength (see Hyndman`s chapter 5)
          tsFtrend = max(0,1-var(remainder(ts_MED_decomp))/var(trendcycle(ts_MED_decomp)+remainder(ts_MED_decomp)))
          # Detrend the series
          coeff <- polyfit(c(1:length(ts_MED_train_deseason)),ts_MED_train_deseason, n=1)
          ts_MED_deseason_detrended <- ts_MED_train_deseason - ((coeff[1]*c(1:length(ts_MED_train_deseason))) + coeff[2])
        } 
        else {
          ts_MED_deseason_detrended <- ts_MED_train_deseason
        }
        
        
        # Fit models and forecasting
        models <- statModels(input = ts_MED_train, 
                             des_input = ts_MED_train_deseason, 
                             #diff_input = ts_MED_train_differenced,
                             des_det_input = ts_MED_deseason_detrended,
                             fh = fh)  
        
        # Residuals diagnostic (TBD)
        #diagnostic <- checkresiduals(models[[nmodels]]$)
        
        
        #Adjust Forecasts and calculate MAE
        for (nmodels in 1:length(namesModels)){
          
          # Add seasonality back to forecasts: point and IC(50,60,70,80,95)
          if (nmodels %in% c(3,4,5,6)) { # SES, Holt-Linear, Holt-Damped and Theta
            models[[nmodels]]$mean <- models[[nmodels]]$mean + SIout
            models[[nmodels]]$lower <- models[[nmodels]]$lower + cbind(SIout,SIout,SIout,SIout,SIout)
            models[[nmodels]]$upper <- models[[nmodels]]$upper + cbind(SIout,SIout,SIout,SIout,SIout)
            models[[nmodels]]$x <- ts_MED_train
          } 
          
          # MAE
          if (nmodels != length(namesModels)) { # all models except LSTM
            # Update matrix of forecasts
            totalFCAST[,,nmodels] <- apply(as.matrix(as.data.frame(models[[nmodels]])),2,round)
            
            # Mean Absolute Error
            totalMAE[nmodels,] <- mae_cal(ts_MED_test, models[[nmodels]]$mean)

          } 
          else {
            # LSTM MAE (from CSV files)
            switch(quarter,
                     totalMAE[length(namesModels),] <- lstm_mae[med,],
                     totalMAE[length(namesModels),] <- lstm_mae[med+nmed,],
                     totalMAE[length(namesModels),] <- lstm_mae[med+2*nmed,]
                   )
            # totalFCAST[,,length(namesModels)] <- lstm_fcast_ic_60
          }

        }

        # define row and column labels of totalMAE and totalFCAST
        dimnames(totalMAE) <- list(namesModels, str)
        dimnames(totalFCAST) <- list(str[1:fh], 
                                     c("Point", "Lo 50", "Hi 50", "Lo 60", "Hi 60", "Lo 70", "Hi 70", 
                                       "Lo 80", "Hi 80", "Lo 95", "Hi 95"),
                                     namesModels)

        # Identify the winner model(s)
        winnerModel = which(totalMAE[,5] == min(totalMAE[,5]))
        
        
        # Calculate aggregate forecasts (Hyndman 12.5 online book)
        for (nmodels in 1:length(namesModels)){
          if (!(nmodels %in% c(1,2,6,10))) { # all models except naive, snaive, theta and lstm
            # Simulate 10000 future sample paths
            fit <- models[[nmodels]]$model
            nsim <- 10000
            sim <- numeric(nsim)
            for(i in seq_len(nsim))
              sim[i] <- sum(simulate(fit, future=TRUE, nsim=fh))
            temp <- cbind(quantile(sim, prob=c(0.25, 0.75)),    # IC 50% 
                          quantile(sim, prob=c(0.2, 0.8)),      # IC 60% 
                          quantile(sim, prob=c(0.15, 0.85)),    # IC 70%
                          quantile(sim, prob=c(0.1, 0.9)),      # IC 80%
                          quantile(sim, prob=c(0.025, 0.975)))  # IC 95%
          } 
          else if ((nmodels %in% c(1,2,6))) {
            temp <- cbind(c(sum(totalFCAST[,,nmodels][,"Lo 50"]), sum(totalFCAST[,,nmodels][,"Hi 50"])),
                          c(sum(totalFCAST[,,nmodels][,"Lo 60"]), sum(totalFCAST[,,nmodels][,"Hi 60"])),
                          c(sum(totalFCAST[,,nmodels][,"Lo 70"]), sum(totalFCAST[,,nmodels][,"Hi 70"])),
                          c(sum(totalFCAST[,,nmodels][,"Lo 80"]), sum(totalFCAST[,,nmodels][,"Hi 80"])),
                          c(sum(totalFCAST[,,nmodels][,"Lo 95"]), sum(totalFCAST[,,nmodels][,"Hi 95"])))
          } 
          else { # LSTM
            # # update aggFCAST with LSTM IC 60 aggregate forecasts (load from .csv file)
            temp <- cbind(c(NA,NA),
                            lstm_fcast_ic_60[med, c(quarter*2-1,quarter*2)],
                            c(NA,NA),
                            c(NA,NA),
                            c(NA,NA))
            
          }
          aggFCAST[,,nmodels] <- round(temp)
          dimnames(aggFCAST[,,nmodels]) <- list(c("Low","High"), c("IC 50", "IC 60", "IC 70", "IC 80", "IC 95"))
        }

        # Update medicines' attributes list
        tsMedResults[[med]] = list(code = xts_list_med[[uf]]$COD_MED[idx_MED,1],
                                   name = xts_list_med[[uf]]$COD_MED[idx_MED,2],
                                   isTrendy = tsHasTrend,
                                   FT = tsFtrend,
                                   isSeasonal = tsHasSeasonality,
                                   FS = tsFseasonality,
                                   isStationary = tsIsStationary,
                                   n_diffs = tsNdiffs,
                                   ns_diffs = tsNSdiffs,
                                   models = models,
                                   nameModels = namesModels,
                                   winnerModel = winnerModel,
                                   agg_predictions = aggFCAST,
                                   predictions = totalFCAST,
                                   predictions_ARIMA_MS = arima_MS_pred[med],
                                   real_demand = sum(ts_MED_test[2:4]),
                                   stock = stockMed[med],
                                   MAE = totalMAE)
        
      } # end medicines loop
      
      # update global results list
      ufResults[[uf]] <- list(UF = UFs_vec[uf],
                              #MAE = totalMAE, 
                              #FCAST = FCAST_med,
                              MED = tsMedResults)
      
      # before movind forward to the next UF empty the medicines list
      tsMedResults <- list()
    
    } # end of UF loop
    
    quarterResults[[quarter]] <- list(Q = quarter,
                                      UF = ufResults)
    
  } # end of Quarter loop
  

  
) # end of system.time


# save results list on disk
saveRDS(quarterResults, "StatModelsResults.rds")


# After this loop we have a list quarterResults[[Q]], where Q = 1 (quarter 1), 2 (quarter 2) or 3 (quarter 3)
# quarterResults[[Q]]$UF[[UFs]] - where UFs = 1("DF"), 2("CE"), 3("PE"), 4("AL"), 5("AP"), 6("TO"), 
#                                             7("PA"), 8("RN"), 9("PB")
#
# quarterResults[[Q]]$UF[[UFs]]$MED[[nMed]] - where nMed = [1,2,...,total of drugs]

# quarterResults[[Q]]$UF[[UFs]]$MED[[nMed]] has several information about the drug:
# $code - code of the drug
# $name - name of the drug
# $isTrendy - drug ts has trend
# $FT - trend strength
# $isSeasonal - whether ts is seasonal or not
# $FS - seasonality strength 
# $isStationary -  whether ts is stationary or not
# $n_diffs - number of differences to make the ts stationary
# $ns_diffs - number of seasonal differences necessary to make the ts stationary
# $models - models fitted
# $nameModels - names of the models ("NAIVE", "SNAIVE", "SES", "HOLT_LINEAR", "HOLT_DAMPED",
#                                    "THETA", "HOLT_WINTERS", "ARIMA", "ETS")
# $winnerModel - model with best MAE
# $predictions - point and IC forecasts
# $MAE - MAE calculation for every model

# Examples:
# quarterResults[[1]]$UF[[1]]$MED[[22]]$MAE - MAE calculation of all methods for Q1, UF="DF", Drug=22
# quarterResults[[1]]$UF[[3]]$MED[[17]]$predictions - forecasts of all methods for Q1, UF="PE", Drug=17
# quarterResults[[1]]$UF[[6]]$MED[[4]]$name - name of the Drug=4 for Q1 and UF="TO"
# quarterResults[[1]]$UF[[2]]$MED[[13]]$models$ARIMA$model - model ARIMA fitted for Q1, UF="CE", Drug=13
# quarterResults[[1]]$UF[[8]]$MED[[16]]$winnerModel - model with best MAE among all models fitted for Drug=16, Q1, UF="RN"